FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 80

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY Instameal.WebApi/Instameal.WebApi.csproj Instameal.WebApi/
COPY Instameal.WebApi/Receipt.db Instameal.WebApi/
COPY Instameal.Models/Instameal.Models.csproj Instameal.Models/
COPY Instameal.Db/Instameal.Db.csproj Instameal.Db/
RUN dotnet restore Instameal.WebApi/Instameal.WebApi.csproj
COPY . .
WORKDIR /src/Instameal.WebApi
RUN dotnet build Instameal.WebApi.csproj -c Release -o /app

# Ensure that we generate and migrate the database 
WORKDIR /src/Instameal.WebApi
RUN dotnet ef database update

#Publish app
FROM build AS publish
RUN dotnet publish --configuration=Release -o /app

# Copy the created database
RUN cp /src/Instameal.WebApi/Receipt.db /app/Receipt.db

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Instameal.WebApi.dll"]
