﻿using System;
using Instameal.Models;

namespace Instameal.Db.Controllers
{
    public class ProductController : BaseController<Product>
    {

        public ProductController(ReceiptContext context)
            : base(context, () => { return context.Products; })
        {

        }
    }
}
