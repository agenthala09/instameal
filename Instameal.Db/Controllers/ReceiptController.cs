﻿using System.Linq;
using Instameal.Models;
using Instameal.Models.Contracts;

namespace Instameal.Db.Controllers
{
    public class ReceiptController : BaseController<Receipt>
    {

        public ReceiptController(ReceiptContext context)
            : base(context, () => { return context.Receipts; })
        {
            Init(context); //init
        }

        private void Init(ReceiptContext context)
        {
            // Look for any movies.
            if (context.Receipts.Any())
            {
                return;   // DB has been seeded
            }

            context.Products.AddRange(
                new Product
                {
                    Name = "Haferflocken"
                },
                new Product
                {
                    Name = "Joghurt"
                },
                new Product
                {
                    Name = "Banane"
                });


            context.Receipts.AddRange(
                 new Receipt
                 {
                     Title = "Healthy Breakfast"
                 });

            context.SaveChanges();

            context.Ingredients.AddRange(
                new Ingredient
                {
                    Amount = "100g",
                    ReceiptId = 1,
                    ProductId = 1
                },
                new Ingredient
                {
                    Amount = "1 geschnitten",
                    ReceiptId = 1,
                    ProductId = 3
                },
                new Ingredient
                {
                    Amount = "250g",
                    ReceiptId = 1,
                    ProductId = 2
                });


            context.SaveChanges();
        }

        public IReceipt[] GetAllReceipts()
        {
            return _set.Select(r => (IReceipt)r).ToArray();
        }
    }
}