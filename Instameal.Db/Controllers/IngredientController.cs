﻿using System;
using System.Linq;
using Instameal.Models;
using Instameal.Models.Contracts;

namespace Instameal.Db.Controllers
{
    public class IngredientController : BaseController<Ingredient>
    {

        public IngredientController(ReceiptContext context)
            : base(context, () => { return context.Ingredients; })
        {

        }

        public IIngredient[] GetIngredientsByReceipt(int receiptId)
        {
            return _set.Where(i => i.ReceiptId == receiptId).ToArray();
        }
    }
}
