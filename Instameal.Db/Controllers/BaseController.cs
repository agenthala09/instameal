﻿using System;
using System.Linq;
using Instameal.Models;
using Instameal.Models.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Instameal.Db
{
    public class BaseController<T>
        where T : Entity
    {
        protected readonly DbContext _context;
        protected readonly DbSet<T> _set;

        public BaseController(DbContext context, Func<DbSet<T>> retrieveDbSet)
        {
            _context = context;
            _set = retrieveDbSet();
        }

        public T Insert(T entity)
        {
            var inserted = _set.Add(entity).Entity;

            _context.SaveChanges();

            return inserted;
        }

        public T Update(int id, T entity)
        {
            var updated = _set.Update(entity).Entity; //todo implement correctly

            _context.SaveChanges();

            return updated;
        }

        public T GetById(int id)
        {
            return _set.SingleOrDefault(i => i.Id == id);
        }

        public T[] GetAll()
        {
            return _set.ToArray();
        }
    }
}