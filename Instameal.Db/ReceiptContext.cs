﻿using System;
using System.Collections.Generic;
using Instameal.Models;
using Microsoft.EntityFrameworkCore;

namespace Instameal.Db
{
    public class ReceiptContext : DbContext
    {
        public ReceiptContext(DbContextOptions<ReceiptContext> options)
            : base(options)
        {
        }

        public DbSet<Receipt> Receipts { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
