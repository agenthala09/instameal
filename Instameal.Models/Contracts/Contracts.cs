﻿using System;
using System.Collections.Generic;

namespace Instameal.Models.Contracts
{
    public interface IDataContract
    {
        int Id { get; set; }
    }

    public interface IReceipt : IDataContract
    {
        string Title { get; set; }
    }

    public interface IIngredient : IDataContract
    {
        string Amount { get; set; }

        int ReceiptId { get; set; }
        int ProductId { get; set; }
    }

    public interface IProduct : IDataContract
    {
        string Name { get; set; }
    }

}