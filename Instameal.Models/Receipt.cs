﻿using System;
using System.Collections.Generic;
using Instameal.Models.Contracts;

namespace Instameal.Models
{
    public class Receipt : Entity, IReceipt
    {
        public string Title { get; set; }

        public ICollection<Ingredient> Ingredients { get; set; }
    }

    public class Ingredient : Entity, IIngredient
    {
        public string Amount { get; set; }

        public int ReceiptId { get; set; }
        public Receipt Receipt { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }
    }

    public class Product : Entity, IProduct
    {
        public string Name { get; set; }

    }
}
