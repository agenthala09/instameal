﻿using System;
using Instameal.Models.Contracts;

namespace Instameal.Models
{
    public class Entity : IDataContract
    {
        public int Id { get; set; }
    }
}
