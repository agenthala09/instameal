﻿using System;
using Instameal.Models.Contracts;
using Newtonsoft.Json;

namespace Instameal.ApiModel
{
    [JsonObject]
    public class Receipt : IReceipt
    {
        [JsonProperty]
        public int Id { get; set; }

        [JsonProperty]
        public string Title { get; set; }

        [JsonProperty]
        public Product[] Products { get; set; }
    }

    [JsonObject]
    public class Product : IProduct
    {
        [JsonProperty]
        public int Id { get; set; }

        [JsonProperty]
        public string Amount { get; set; }

        [JsonProperty]
        public string Name
        {
            get;
            set;
        }
    }
}
