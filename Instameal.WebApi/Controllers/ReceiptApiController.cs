﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Instameal.Db;
using Instameal.Managers;
using Instameal.ApiModel;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Instameal.Controllers
{
    [Route("api/receipt")]
    [ApiController]
    public class ReceiptApiController : ControllerBase
    {
        public ReceiptManager ReceiptManager
        {
            get;
            set;
        }

        public ReceiptApiController(ReceiptContext context)
        {
            ReceiptManager = new ReceiptManager(context);
        }

        // GET: api/receipts
        [HttpGet]
        public IEnumerable<Receipt> Get()
        {
            return ReceiptManager.GetAllReceipts();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Receipt Get(int id)
        {
            return ReceiptManager.GetReceiptById(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
