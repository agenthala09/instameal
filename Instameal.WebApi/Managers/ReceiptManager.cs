﻿using System;
using System.Collections.Generic;
using System.Linq;
using Instameal.ApiModel;
using Instameal.Db;
using Instameal.Db.Controllers;


namespace Instameal.Managers
{
    public class ReceiptManager
    {
        private readonly ReceiptController _receiptController;
        private readonly IngredientController _ingredientController;
        private readonly ProductController _productController;

        public ReceiptManager(ReceiptContext context)
        {
            //Init
            _receiptController = new ReceiptController(context);
            _ingredientController = new IngredientController(context);
            _productController = new ProductController(context);
        }

        public IEnumerable<Receipt> GetAllReceipts()
        {
            List<Receipt> receipts = new List<Receipt>();

            foreach (var receipt in _receiptController.GetAll())
            {
                receipts.Add(new Receipt() { Id = receipt.Id, Title = receipt.Title, Products = GetProductsByReceipt(receipt.Id) });
            }

            return receipts;
        }

        public Receipt GetReceiptById(int id)
        {
            var receipt = _receiptController.GetById(id);

            return new Receipt() { Id = receipt.Id, Title=receipt.Title, Products = GetProductsByReceipt(receipt.Id) };
        }

        internal Product[] GetProductsByReceipt(int receiptId)
        {
            List<Product> products = new List<Product>();
            foreach (var ing in _ingredientController.GetIngredientsByReceipt(receiptId))
            {
                var product = _productController.GetById(ing.ProductId);

                //Convert to model
                var prodModel = new Product { Id = product.Id, Amount = ing.Amount, Name = product.Name };
                products.Add(prodModel);
            }

            return products.ToArray();
        }
    }
}
